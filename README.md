## Paint-gtk-sdl
![paint-gtk-sdl](https://gitlab.com/Energy1011/paint-gtk-sdl/-/raw/master/data/images/screenshot.png)

### Install
```bash
autoreconf -fvi
./configure
make && make install
```

## Dependencias
```bash
#For apt
apt install build-essential
apt install libsdl-dev
apt install libgtk2.0-dev
```