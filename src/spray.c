/*
 * spray.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <stdlib.h>

#include <stdarg.h>

#include "api-paint.h"
#include "spray.h"

void spray_mouse_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (estado->active) return;
	
	/* Procesar este método */
	estado->active = 1;
	
	estado->x1 = evento->button.x;
	estado->y1 = evento->button.y;
	spray (dibujo, frente, estado->x1, estado->y1, 30); /* FIXME: Editar esta constante */
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
}

void spray_mouse_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!estado->active) return;
	
	spray (dibujo, frente, evento->button.x, evento->button.y, 30); /* FIXME: Editar esta constante */
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
	
	estado->active = 0;
}

void spray_mouse_motion (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (estado->active) {
		if (evento->motion.xrel == 0 && evento->motion.yrel == 0) {
			estado->x1 = evento->button.x;
			estado->y1 = evento->button.y;
			spray (dibujo, frente, estado->x1, estado->y1, 30);
		} else {
			bresenham_ejecutable (dibujo, frente, evento->motion.x - evento->motion.xrel, evento->motion.y - evento->motion.yrel, evento->motion.x, evento->motion.y, spray_v, 30, estado); /*FIXME: Editar esta constante */
		}
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
	}
	/* TODO: Dibujar el spray */
}

void spray_timer (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (!estado->active) return;
	spray (dibujo, frente, estado->x1, estado->y1, 30); /* FIXME: La maldita constante */
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
}

void spray_v (SDL_Surface *screen, Uint32 color, int x, int y, va_list args) {
	/* Los dos argumentos variables que recibimos es:
	 * int tamaño: El tamaño del spray
	 * DrawState estado: La estructura del estado
	 */
	int tam;
	DrawState *estado;
	
	tam = va_arg (args, int);
	estado = va_arg (args, DrawState *);
	
	spray (screen, color, x, y, tam);
	estado->x1 = x;
	estado->y1 = y;
}

void spray (SDL_Surface *screen, Uint32 color, int x, int y, int tam) {
	int g, h, i;
	
	for (g = -tam; g < tam; g++) {
		for (h = -tam; h < tam; h++) {
			if ((g * g + h * h) >= (tam * tam)) continue;
			
			i = 1 + (int) (150.0 * rand () / (RAND_MAX + 1.0));
			if (i > 149) drawpixel (screen, color, x + g, y + h);
		}
	}
}

