/*
 * circle.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>

#include "api-paint.h"
#include "circle.h"

void recalc_circle (DrawState *draw_data) {
	int dx, dy;
	
	dx = draw_data->x_final - draw_data->x_initial;
	dy = draw_data->y_final - draw_data->y_initial;
	
	draw_data->radio_x = dx;
	draw_data->radio_y = dy;
	
	if (draw_data->mode & CIRCLE) {
		if (abs (dx) > abs (dy)) {
			draw_data->radio_x = draw_data->radio_y = abs (dx);
		} else {
			draw_data->radio_x = draw_data->radio_y = abs (dy);
		}
		
		if ((draw_data->mode & CENTER) == 0) {
			if (dx < 0) draw_data->radio_x = -draw_data->radio_x;
			if (dy < 0) draw_data->radio_y = -draw_data->radio_y;
		}
	}
	
	if (draw_data->mode & CENTER) {
		draw_data->radio_x = abs (draw_data->radio_x);
		draw_data->radio_y = abs (draw_data->radio_y);
		draw_data->x1 = draw_data->x_initial;
		draw_data->y1 = draw_data->y_initial;
	} else {
		draw_data->radio_x /= 2;
		draw_data->radio_y /= 2;
		draw_data->x1 = draw_data->x_initial + draw_data->radio_x;
		draw_data->y1 = draw_data->y_initial + draw_data->radio_y;
	}
	
	draw_data->radio_x = abs (draw_data->radio_x);
	draw_data->radio_y = abs (draw_data->radio_y);
}

void circle_mouse_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *draw_data) {
	SDLMod mod;
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (draw_data->active) return;
	
	/* Procesar este método */
	draw_data->active = 1;
	
	draw_data->x_initial = draw_data->x_final = evento->button.x;
	draw_data->y_initial = draw_data->y_final = evento->button.y;
	draw_data->radio_x = draw_data->radio_y = 0;
	
	draw_data->mode = 0;
	
	mod = SDL_GetModState ();
	
	if (mod & KMOD_SHIFT) {
		draw_data->mode |= CIRCLE;
	}
	
	if (mod & KMOD_CTRL) {
		draw_data->mode |= CENTER;
	}
	
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
	circle (screen, frente, draw_data->x_initial, draw_data->y_initial, draw_data->radio_x);
}

void circle_mouse_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *draw_data) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = evento->button.x;
	draw_data->y_final = evento->button.y;
	
	recalc_circle (draw_data);
	
	if (draw_data->mode & CIRCLE) {
		circle (dibujo, frente, draw_data->x1, draw_data->y1, draw_data->radio_x);
	} else {
		elipse (dibujo, frente, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->radio_y);
	}
	draw_data->active = 0;
	
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
}

void circle_mouse_motion (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *draw_data) {
	if ((evento->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = evento->motion.x;
	draw_data->y_final = evento->motion.y;
	
	recalc_circle (draw_data);
	
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
	if (draw_data->mode & CIRCLE) {
		circle (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x);
	} else {
		elipse (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->radio_y);
	}
}

void circle_key_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *draw_data) {
	SDLKey tecla;
	int touched = 0;
	tecla = evento->key.keysym.sym;
	
	switch (tecla) {
		case SDLK_RCTRL:
		case SDLK_LCTRL:
			draw_data->mode |= CENTER;
			touched = 1;
			break;
		case SDLK_RSHIFT:
		case SDLK_LSHIFT:
			draw_data->mode |= CIRCLE;
			touched = 1;
			break;
		default: /* Para evitar warnings */
			break;
	}
	
	if (touched && draw_data->active) {
		recalc_circle (draw_data);
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		if (draw_data->mode & CIRCLE) {
			circle (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x);
		} else {
			elipse (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->radio_y);
		}
	}
	
}

void circle_key_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *draw_data) {
	SDLKey tecla;
	int touched = 0;
	tecla = evento->key.keysym.sym;
	
	switch (tecla) {
		case SDLK_RCTRL:
		case SDLK_LCTRL:
			draw_data->mode &= ~CENTER;
			touched = 1;
			break;
		case SDLK_RSHIFT:
		case SDLK_LSHIFT:
			draw_data->mode &= ~CIRCLE;
			touched = 1;
			break;
	}
	if (touched && draw_data->active) {
		recalc_circle (draw_data);
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		if (draw_data->mode & CIRCLE) {
			circle (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x);
		} else {
			elipse (screen, frente, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->radio_y);
		}
	}
}

void circle (SDL_Surface *surface, Uint32 color, int xc, int yc, int r) {
	int p;
	int x, y;
	
	p = 1 - r;
	x = 0;
	y = r;
	
	drawpixel (surface, color, xc + x, yc + y);
	drawpixel (surface, color, xc - x, yc + y);
	drawpixel (surface, color, xc + x, yc - y);
	drawpixel (surface, color, xc - x, yc - y);
	drawpixel (surface, color, xc + y, yc + x);
	drawpixel (surface, color, xc - y, yc + x);
	drawpixel (surface, color, xc + y, yc - x);
	drawpixel (surface, color, xc - y, yc - x);
	
	while (x < y) {
		if (p < 0) {
			x++;
			
			p = p + (2 * x) + 1;
		} else {
			x++;
			y--;
			p = p + (2 * x) + 1 - (2 * y);
		}
		drawpixel (surface, color, xc + x, yc + y);
		drawpixel (surface, color, xc - x, yc + y);
		drawpixel (surface, color, xc + x, yc - y);
		drawpixel (surface, color, xc - x, yc - y);
		drawpixel (surface, color, xc + y, yc + x);
		drawpixel (surface, color, xc - y, yc + x);
		drawpixel (surface, color, xc + y, yc - x);
		drawpixel (surface, color, xc - y, yc - x);
	}
}

void elipse (SDL_Surface *surface, Uint32 color, int xc, int yc, int rx, int ry) {
	double p;
	double doble_rx, doble_ry;
	double x, y;
	
	x = 0; y = ry;
	drawpixel (surface, color, xc + x, yc + y);
	drawpixel (surface, color, xc + x, yc - y);
	
	doble_rx = 2.0 * rx * rx;
	doble_ry = 2.0 * ry * ry;
	
	p = (ry * ry) - (rx * rx * ry) + (rx * rx) / 4.0;
	
	while ((doble_ry * x) < (doble_rx * y)) {	
		x++;
		
		if (p < 0) {
			p = p + (doble_ry * x) + (ry * ry);
		} else {
			y--;
			p = p + (doble_ry * x) - (doble_rx * y) + (ry * ry);
		}
		
		drawpixel (surface, color, (int) xc + x, (int) yc + y);
		drawpixel (surface, color, (int) xc - x, (int) yc + y);
		drawpixel (surface, color, (int) xc + x, (int) yc - y);
		drawpixel (surface, color, (int) xc - x, (int) yc - y);
	}
	
	p = (((double) ry * (double) ry) * ((x + 0.5) * (x + 0.5))) + (((double) rx * (double) rx) * ((y - 1.0) * (y - 1.0))) - ((double) rx * (double) rx * (double) ry * (double) ry);
	
	while (y > 0) {
		y--;
		if (p > 0.0) {
			p = p - (doble_rx * y) + (rx * rx);
		} else {
			x++;
			p = p + (doble_ry * x) - (doble_rx * y) + (rx * rx);
		}
		drawpixel (surface, color, (int) xc + x, (int) yc + y);
		drawpixel (surface, color, (int) xc - x, (int) yc + y);
		drawpixel (surface, color, (int) xc + x, (int) yc - y);
		drawpixel (surface, color, (int) xc - x, (int) yc - y);
	}
}

