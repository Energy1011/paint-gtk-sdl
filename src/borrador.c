/*
 * borrador.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <stdarg.h>

#include "api-paint.h"
#include "borrador.h"

void borrador_mouse_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (estado->active) return;
	
	/* Procesar este método */
	estado->active = 1;
	
	borrar (dibujo, fondo, evento->button.x, evento->button.y, 6); /* FIXME: Editar esta constante */
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
}

void borrador_mouse_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!estado->active) return;
	
	borrar (dibujo, fondo, evento->button.x, evento->button.y, 6); /* FIXME: Editar esta constante */
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
	
	estado->active = 0;
}

void borrador_mouse_motion (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (estado->active) {
		if (evento->motion.xrel == 0 && evento->motion.yrel == 0) {
			borrar (dibujo, fondo, evento->button.x, evento->button.y, 6);
		} else {
			bresenham_ejecutable (dibujo, fondo, evento->motion.x - evento->motion.xrel, evento->motion.y - evento->motion.yrel, evento->motion.x, evento->motion.y, borrar_v, 6); /*FIXME: Editar esta constante */
		}
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
	}
	/* TODO: Dibujar el cuadro del borrador */
}

void borrar_v (SDL_Surface *screen, Uint32 color, int x, int y, va_list args) {
	/* El único argumento variable que recibimos es:
	 * int tamaño: El tamaño del borrador
	 */
	int tam;
	tam = va_arg (args, int);
	borrar (screen, color, x, y, tam);
}

void borrar (SDL_Surface *screen, Uint32 color, int x, int y, int tam) {
	SDL_Rect rect;
	
	rect.x = x - (tam / 2);
	rect.y = y - (tam / 2);
	rect.h = rect.w = tam;
	
	SDL_FillRect (screen, &rect, color);
}

