/*
 * api-paint.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>

#include "api-paint.h"

void drawpixel (SDL_Surface *surface, Uint32 color, int x, int y) {
	Uint8 *p;
	int bpp;
	// y = SCREEN_HEIGHT - y;
	if (x < 0 || y < 0 || y >= SCREEN_HEIGHT || x >= SCREEN_WIDTH) return;
	
	bpp = surface->format->BytesPerPixel;
	p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	SDL_LockSurface (surface);
	switch (bpp) {
		case 1:
			*p = color;
			break;
		case 2:
			*(Uint16 *) p = color;
			break;
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				p[0] = (color >> 16) & 0xFF;
				p[1] = (color >> 8) & 0xFF;
				p[2] = color & 0xFF;
			} else {
				p[2] = (color >> 16) & 0xFF;
				p[1] = (color >> 8) & 0xFF;
				p[0] = color & 0xFF;
			}
			break;
		case 4:
			*(Uint32 *) p = color;
			break;
	}
	SDL_UnlockSurface (surface);
}

Uint32 getpixel (SDL_Surface *surface, int x, int y) {
	Uint8 *p;
	int bpp, pos;
	
	if (x < 0 || y < 0 || y >= SCREEN_HEIGHT || x >= SCREEN_WIDTH) return 0;
	bpp = surface->format->BytesPerPixel;
	pos = (y * surface->pitch) / bpp + x;
	
	switch (bpp) {
		case 1:
			return ((Uint8 *) surface->pixels)[pos];
			break;
		case 2:
			return ((Uint16 *) surface->pixels)[pos];
			break;
		case 3:
			pos = pos + (2 * x);
			if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				return ((Uint8 *) surface->pixels)[pos] << 16 | ((Uint8 *) surface->pixels)[pos + 1] << 8 | ((Uint8 *) surface->pixels)[pos + 2];
			} else {
				return ((Uint8 *) surface->pixels)[pos + 2] << 16 | ((Uint8 *) surface->pixels)[pos + 1] << 8 | ((Uint8 *) surface->pixels)[pos];
			}
			break;
		case 4:
			return ((Uint32 *) surface->pixels)[pos];
			break;
	}
}

/* #define SDL_ALPHA_OPAQUE 255
#define SDL_ALPHA_TRANSPARENT 0 */
#define ALPHA_BLEND(sR, sG, sB, A, dR, dG, dB)  \
do {                                            \
        dR = (((sR-dR)*(A)+255)>>8)+dR;         \
        dG = (((sG-dG)*(A)+255)>>8)+dG;         \
        dB = (((sB-dB)*(A)+255)>>8)+dB;         \
} while(0)
void drawpixel_trans (SDL_Surface *surface, Uint32 color, int x, int y, double a) {
	void *pixel;
	int bpp, alpha;
	Uint8 r, g, b;
	Uint8 sr, sg, sb;
	Uint32 source_pixel;
	a = a * 255;
	alpha = 255 - a;
	if (x < 0 || y < 0 || y > SCREEN_HEIGHT || x > SCREEN_WIDTH) return;
	/*y = SCREEN_HEIGHT - y;*/
	
	bpp = surface->format->BytesPerPixel;
	pixel = surface->pixels + (y * surface->pitch) + (x * bpp);
	
	if (SDL_MUSTLOCK(surface)) {
		SDL_LockSurface (surface);
	}
	switch (bpp) {
		case 1:
			fprintf (stderr, "Advertencia: No se puede dibujar transparencias sobre una superficie indexada\n");
			break;
		case 2:
			SDL_GetRGB (color, surface->format, &r, &g, &b);
			source_pixel = *(Uint16 *) pixel;
			SDL_GetRGB (source_pixel, surface->format, &sr, &sg, &sb);
			
			ALPHA_BLEND (sr, sg, sb, alpha, r, g, b);
			
			color = SDL_MapRGB (surface->format, r, g, b);
			*(Uint16 *) pixel = (Uint16) color;
			break;
		case 3:
			SDL_GetRGB (color, surface->format, &r, &g, &b);
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				sr = ((Uint8 *) pixel)[0];
				sg = ((Uint8 *) pixel)[1];
				sb = ((Uint8 *) pixel)[2];
			} else {
				sr = ((Uint8 *) pixel)[2];
				sg = ((Uint8 *) pixel)[1];
				sb = ((Uint8 *) pixel)[0];
			}
			
			ALPHA_BLEND (sr, sg, sb, alpha, r, g, b);
			
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN) {
				((Uint8 *) pixel)[0] = r;
				((Uint8 *) pixel)[1] = g;
				((Uint8 *) pixel)[2] = b;
			} else {
				((Uint8 *) pixel)[2] = b;
				((Uint8 *) pixel)[1] = g;
				((Uint8 *) pixel)[0] = r;
			}
			break;
		case 4:
			SDL_GetRGB (color, surface->format, &r, &g, &b);
			source_pixel = *(Uint32 *) pixel;
			SDL_GetRGB (source_pixel, surface->format, &sr, &sg, &sb);
			
			ALPHA_BLEND (sr, sg, sb, alpha, r, g, b);
			color = SDL_MapRGB (surface->format, r, g, b);
			*(Uint32 *) pixel = (Uint32) color;
			break;
	}
	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface (surface);
	}
}

