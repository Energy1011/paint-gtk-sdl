/*
 * paint-gtk-sdl.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include <SDL.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "glib-sdl.h"
#include "api-paint.h"

#include "line.h"
#include "circle.h"
#include "rect.h"
#include "spline.h"
#include "polygon.h"
#include "fill.h"
#include "borrador.h"
#include "mano.h"
#include "spray.h"
#include "recorte.h"

enum {
	KEYDOWN = 0,
	KEYUP,
	MOUSEDOWN,
	MOUSEUP,
	MOUSEMOTION,
	TIMER,
	NUM_EVENTS
};

const char *img_toolbar_buttons_names [NUM_MODES] = {
	DATA_DIR "images/button-cursor.png",
	DATA_DIR "images/button-line.png",
	DATA_DIR "images/button-circle-elipse.png",
	DATA_DIR "images/button-rect.png",
	DATA_DIR "images/button-poly.png",
	DATA_DIR "images/button-spline.png",
	DATA_DIR "images/button-pencil.png",
	DATA_DIR "images/button-erase.png",
	DATA_DIR "images/button-spray.png",
	DATA_DIR "images/button-fill.png",
	DATA_DIR "images/button-cut.png",
};

GtkWidget *main_window;
GtkWidget *label_posxy;

SDL_Surface *main_screen;
SDL_Surface *draw_screen;

int mode;

DrawState *draw_data;
Uint32 color_bg, color_fg;
GtkWidget *color_button_fg, *color_button_bg;

/* Reg all functions */
/* Keydown, Keyup, MouseDown, MouseUp, Mouse Motion */
PaintFunc functions [NUM_MODES][NUM_EVENTS] = {
/* Nada */		 {NULL, NULL, NULL, NULL, NULL, NULL},
/*Line*/		 {NULL, NULL, line_mouse_down, line_mouse_up, line_mouse_motion, NULL},
/*Circle-Elipse*/{circle_key_down, circle_key_up, circle_mouse_down, circle_mouse_up, circle_mouse_motion, NULL},
/*Rect*/		 {NULL, NULL, rect_mouse_down, rect_mouse_up, rect_mouse_motion, NULL},
/*Polygon*/      {polygon_key_down, polygon_key_up, polygon_mouse_down, polygon_mouse_up, polygon_mouse_motion, NULL},
/*Spline*/		 {NULL, NULL, spline_mouse_down, spline_mouse_up, spline_mouse_motion, NULL},
/* Lápiz */		 {NULL, NULL, mano_alzada_mouse_down, mano_alzada_mouse_up, mano_alzada_mouse_motion, NULL},
/* Borrador */	 {NULL, NULL, borrador_mouse_down, borrador_mouse_up, borrador_mouse_motion, NULL},
/* Spray */		 {NULL, NULL, spray_mouse_down, spray_mouse_up, spray_mouse_motion, spray_timer},
/*Fill*/		 {NULL, NULL, fill_mouse_down, fill_mouse_up, NULL, NULL},
/* Recortes */	 {NULL, NULL, recorte_mouse_down, recorte_mouse_up, recorte_mouse_motion, NULL}
};

void about_dialog(GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog;
	
	dialog = gtk_about_dialog_new();
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), "Paint GTK-SDL");
 	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "1.0"); 
  	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog), "Este programa está bajo los terminos de GNU General Public License 3\n (c) Copyright 2013, Anaya Padilla Noe Alejandro, \n Arreola Rodríguez Félix");
	gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), "Paint GTK-SDL, Programa sencillo para dibujar.");
  	gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), "https://github.com/Energy1011/Paint-Gtk-SDL");
	gtk_dialog_run(GTK_ABOUT_DIALOG (dialog));
	gtk_widget_destroy (dialog);

}

void save_file()
{
	GtkWidget *dialog;
	GtkFileFilter *filter;
	gint res;
	char *name;
	
	dialog = gtk_file_chooser_dialog_new ("Save image file...", NULL, GTK_FILE_CHOOSER_ACTION_SAVE, "Guardar", GTK_RESPONSE_OK, "Cancelar", GTK_RESPONSE_CANCEL, NULL);
	filter = gtk_file_filter_new ();
	
	gtk_file_filter_set_name (filter, "Imágenes");
	gtk_file_filter_add_pixbuf_formats (filter);
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res != GTK_RESPONSE_OK) {
		gtk_widget_destroy (dialog);
		return;
	}
	
	name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

	SDL_SaveBMP(draw_screen,name);
	printf("%s File saved.\n", name);
	
	g_free (name);
	gtk_widget_destroy (dialog);

}

void open_file(GtkToolItem *toolitem, gpointer data)
{
	GtkWidget *dialog;
	GtkFileFilter *filter;
	gchar *name;
	GdkPixbuf *pixbuf;
	gint res;
	SDL_Surface *image_surface;

	dialog = gtk_file_chooser_dialog_new ("Open image file...", NULL, GTK_FILE_CHOOSER_ACTION_OPEN, "Abrir", GTK_RESPONSE_OK, "Cancelar", GTK_RESPONSE_CANCEL, NULL);
	filter = gtk_file_filter_new ();

	gtk_file_filter_set_name (filter, "Imágenes");
	gtk_file_filter_add_pixbuf_formats (filter);
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	
	if (res != GTK_RESPONSE_OK) {
		gtk_widget_destroy (dialog);
		return;
	}
	
	name = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
	printf("name=%s\n",name);

	pixbuf = gdk_pixbuf_new_from_file (name, NULL);
	image_surface = SDL_CreateRGBSurface (main_screen->flags | SDL_SWSURFACE, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf), main_screen->format->BitsPerPixel, main_screen->format->Rmask, main_screen->format->Gmask, main_screen->format->Bmask, main_screen->format->Amask);

	image_surface = SDL_LoadBMP(name);
	SDL_BlitSurface(image_surface, NULL, draw_screen, NULL);	
	SDL_BlitSurface(draw_screen, NULL, main_screen, NULL);	

	printf("%s Image loaded width=%i height=%i\n",name ,gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));

	SDL_FreeSurface(image_surface);
	g_free (name);
	gtk_widget_destroy (dialog);
}

void new_file()
{
	static gboolean dialog_running;
	if (!dialog_running){
		GtkWidget *dialog;
		gint dialog_response;
	 	dialog = gtk_message_dialog_new(NULL,0,GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "¿ Está seguro de crear un nuevo archivo ?");
	 	dialog_running = TRUE;
	 	dialog_response = gtk_dialog_run(GTK_DIALOG(dialog));
	 	switch(dialog_response)
	 	{
	 		case GTK_RESPONSE_YES:
				SDL_FillRect(draw_screen, NULL, color_bg);
				SDL_BlitSurface(draw_screen, NULL, main_screen, NULL);
				gtk_widget_destroy(dialog);
				dialog_running = FALSE;
	 		break;

	 		default:
		 		gtk_widget_destroy(dialog);
		 		dialog_running = FALSE;
	 		break;
	 	}
	}
}

void change_mode(GtkToggleToolButton *toggle, gpointer e){
	if (!gtk_toggle_tool_button_get_active (toggle)) return;
	
	/* TODO: Ejecutar aquí la función de inicialización de cada uno de las herramientas */
	mode = GPOINTER_TO_INT (e);
}

void exit_failure(const char *reason)
{
	fprintf (stderr, "%s\n", reason);
	glib_sdl_quit ();
	gtk_main_quit ();
	exit (EXIT_FAILURE);
}

gboolean destroy (GtkWidget *widget, GdkEvent *event, gpointer data) {
	static gboolean dialog_running;
	if (!dialog_running){
		GtkWidget *dialog;
		gint dialog_response;
	 	dialog = gtk_message_dialog_new(NULL,0,GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "¿ Desea salir del programa ?");
	 	dialog_running = TRUE;
	 	dialog_response = gtk_dialog_run(GTK_DIALOG(dialog));
	 	switch(dialog_response)
	 	{
	 		case GTK_RESPONSE_YES:
				glib_sdl_quit ();
				gtk_main_quit ();
				gtk_widget_destroy(dialog);
				dialog_running = FALSE;
				exit (EXIT_SUCCESS);
				return FALSE;
	 		break;

	 		default:
		 		gtk_widget_destroy(dialog);
		 		dialog_running = FALSE;
		 		return TRUE;
	 		break;
	 	}
	}
}

gboolean sdl_event (SDL_Event *event, gpointer data) {
	SDLKey tecla;
	gchar *str;
	// Update labels
	if (event->type == SDL_MOUSEMOTION) {
		str = g_strdup_printf ("X:%i Y:%i", event->motion.x, event->motion.y);
		gtk_label_set_text (GTK_LABEL (label_posxy), str);
		g_free (str);
	}
	// Main SDL event cases
	switch (event->type) {
		case SDL_QUIT:
			destroy(NULL, NULL, NULL);
			break;
		case SDL_MOUSEBUTTONDOWN:
			if (functions [mode][MOUSEDOWN] != NULL) {
				functions [mode][MOUSEDOWN] (main_screen, draw_screen, color_fg, color_bg, event, draw_data);
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (functions [mode][MOUSEUP] != NULL) {
				functions [mode][MOUSEUP] (main_screen, draw_screen, color_fg, color_bg, event, draw_data);
			}
			break;
		case SDL_MOUSEMOTION:
			if (functions [mode][MOUSEMOTION] != NULL) {

				functions [mode][MOUSEMOTION] (main_screen, draw_screen, color_fg, color_bg, event, draw_data);
			}
			break;
		case SDL_KEYUP:
			tecla = event->key.keysym.sym;
			
			/* Nuestros atajos de teclado tienen precedencia sobre los eventos para las funciones */
			switch (tecla) {
				/* TODO: Poner aquí nuestras teclas */
				default:
					if (functions [mode][KEYUP] != NULL) {
						functions [mode][KEYUP] (main_screen, draw_screen, color_fg, color_bg, event, draw_data);
					}
			}
			break;
		case SDL_KEYDOWN:
			tecla = event->key.keysym.sym;
			
			/* Nuestros atajos de teclado tienen precedencia sobre los eventos para las funciones */
			switch (tecla) {
				/* TODO: Poner aquí nuestras teclas */
				case SDLK_ESCAPE:
					/* Cancelar cualquier dibujo, y restaurar la imagen */
					draw_data->active = 0;
					SDL_BlitSurface (draw_screen, NULL, main_screen, NULL);
					break;
				default:
					if (functions [mode][KEYDOWN] != NULL) {
						functions [mode][KEYDOWN] (main_screen, draw_screen, color_fg, color_bg, event, draw_data);
					}
			}
			break;
		case SDL_ACTIVEEVENT:
			if ((event->active.state & 2) == 0) break;
			if (event->active.gain) {
				gtk_window_set_keep_above (GTK_WINDOW (main_window), TRUE);
			} else {
				gtk_window_set_keep_above (GTK_WINDOW (main_window), FALSE);
			}
	}
	//Main flip
	SDL_Flip (main_screen);
	return TRUE;
}

void cb_cambia_color_frente (GtkToolButton *button, gpointer data) {
	/* En la data adicional recibimos el nombre del color */
	const gchar *nombre = (const gchar *) data;
	GdkColor color;
	Uint8 r, g, b;
	
	gdk_color_parse (nombre, &color);
	gtk_color_button_set_color (GTK_COLOR_BUTTON (color_button_fg), &color);
	
	/* Ahora el color con SDL */
	r = (color.red >> 8);
	g = (color.green >> 8);
	b = (color.blue >> 8);
	
	color_fg = SDL_MapRGB (main_screen->format, r, g, b);
}

void cb_cambia_color_fondo (GtkToolButton *button, GdkEventButton *evento, gpointer data) {
	/* En la data adicional recibimos el nombre del color */
	const gchar *nombre = (const gchar *) data;
	GdkColor color;
	Uint8 r, g, b;
	
	printf ("Se ejecuta\n"); /*FIXME: Este evento no funciona */
	if (evento->type == GDK_BUTTON_PRESS && evento->button == 3) {
		gdk_color_parse (nombre, &color);
		gtk_color_button_set_color (GTK_COLOR_BUTTON (color_button_bg), &color);
		
		/* Ahora el color con SDL */
		r = (color.red >> 8);
		g = (color.green >> 8);
		b = (color.blue >> 8);

		color_bg = SDL_MapRGB (main_screen->format, r, g, b);
	}
}

void cb_color_frente (GtkColorButton *button, gpointer data) {
	Uint8 r, g, b;
	GdkColor color;
	
	gtk_color_button_get_color (button, &color);
	
	r = (color.red >> 8);
	g = (color.green >> 8);
	b = (color.blue >> 8);

	color_fg = SDL_MapRGB (main_screen->format, r, g, b);
}

void cb_color_fondo (GtkColorButton *button, gpointer data) {
	Uint8 r, g, b;
	GdkColor color;
	
	gtk_color_button_get_color (button, &color);
	
	r = (color.red >> 8);
	g = (color.green >> 8);
	b = (color.blue >> 8);

	color_bg = SDL_MapRGB (main_screen->format, r, g, b);
}

gboolean cb_timer (gpointer data) {
	if (functions [mode][TIMER] != NULL) {
		functions [mode][TIMER] (main_screen, draw_screen, color_fg, color_bg, NULL, draw_data);
	}
	
	SDL_Flip (main_screen);
	return TRUE;
}

/* Colores disponibles en la paleta
 * Para una lista completa de nombres, ver /etc/X11/rgb.txt
 */
static const gchar * colores[] = {
	"white",
	"black",
	"gray",
	"blue",
	"cyan",
	"green",
	"yellow",
	"red",
	"magenta",
	"purple",
	"orange",
	"brown",
	"pink",
	"violet",
	"LimeGreen",
	"beige",
	"chocolate",
	"lemon chiffon",
	NULL
};

void setup_toolbar (GtkWidget *vbox){
	int e;
	GtkWidget *load_image, *hbox;
	GtkToolItem *radiobutton, *toolcolor, *button;
	GtkWidget *paleta, *toolgroup;
	GdkColor color;
	GdkPixbuf *pixbuf;
	guint32 pixel;
// FIXME: this can be translatable text, with gettext.
	const char *toolbar_buttons_names [NUM_MODES] = {
		"Cursor",
		"Línea",
		"Círculo y Elipse",
		"Rectángulo",
		"Polígono",
		"Línea curva (Spline)",
		"Lápiz (Mano alzada)",
		"Borrador",
		"Aerosol (Spray)",
		"Herramienta de relleno (Fill)",
		"Herramienta de recorte",
	};
	
	paleta = gtk_tool_palette_new ();
	gtk_tool_palette_set_style (GTK_TOOL_PALETTE (paleta), GTK_TOOLBAR_ICONS);
	gtk_box_pack_start (GTK_BOX (vbox), paleta, TRUE, TRUE, 0);
	
	toolgroup = gtk_tool_item_group_new ("Archivo");
	gtk_container_add (GTK_CONTAINER (paleta), toolgroup);

	radiobutton = NULL;

	button = gtk_tool_button_new_from_stock(GTK_STOCK_NEW);
	gtk_widget_set_tooltip_text (GTK_WIDGET (button), "Nuevo");
	g_signal_connect (button, "clicked", G_CALLBACK (new_file), NULL);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), button, -1);

	button = gtk_tool_button_new_from_stock(GTK_STOCK_OPEN);
	gtk_widget_set_tooltip_text (GTK_WIDGET (button), "Abrir");
	g_signal_connect (button, "clicked", G_CALLBACK (open_file), NULL);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), button, -1);


	button = gtk_tool_button_new_from_stock(GTK_STOCK_SAVE);
	gtk_widget_set_tooltip_text (GTK_WIDGET (button), "Guardar");
	g_signal_connect (button, "clicked", G_CALLBACK (save_file), NULL);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), button, -1);

	button = gtk_tool_button_new_from_stock(GTK_STOCK_ABOUT);
	gtk_widget_set_tooltip_text (GTK_WIDGET (button), "Acerca de");
	g_signal_connect (button, "clicked", G_CALLBACK (about_dialog), NULL);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), button, -1);

	toolgroup = gtk_tool_item_group_new ("Dibujo básico");
	gtk_container_add (GTK_CONTAINER (paleta), toolgroup);
	
	
	// Load buttons
	for (e = 0; e < NUM_MODES; e++) {
		radiobutton = gtk_radio_tool_button_new (radiobutton ? gtk_radio_tool_button_get_group (GTK_RADIO_TOOL_BUTTON (radiobutton)) : NULL);
		g_signal_connect (radiobutton, "toggled", G_CALLBACK (change_mode), GINT_TO_POINTER (e));
		
		load_image = gtk_image_new_from_file (img_toolbar_buttons_names[e]);

		gtk_widget_set_tooltip_text (GTK_WIDGET (radiobutton), toolbar_buttons_names[e]);
		
		gtk_tool_button_set_icon_widget (GTK_TOOL_BUTTON (radiobutton), load_image);
		
		gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), radiobutton, -1);
	}
	/* Ahora los colores */
	toolgroup = gtk_tool_item_group_new ("Colores");
	gtk_container_add (GTK_CONTAINER (paleta), toolgroup);
	
	for (e = 0; colores[e] != NULL; e++) {
		
		pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, 22, 22);
		gdk_color_parse (colores[e], &color);
		pixel = ((color.red & 0xff00) << 16) | 
          ((color.green & 0xff00) << 8) | 
           (color.blue & 0xff00);
        gdk_pixbuf_fill (pixbuf, pixel);
        load_image = gtk_image_new_from_pixbuf (pixbuf);
        
        g_object_unref (pixbuf);
		toolcolor = gtk_tool_button_new (load_image, NULL);
		gtk_widget_set_tooltip_text (GTK_WIDGET (toolcolor), colores[e]);
		gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), toolcolor, -1);
		
		g_signal_connect (toolcolor, "clicked", G_CALLBACK (cb_cambia_color_frente), (gpointer)colores[e]);
		g_signal_connect (toolcolor, "button-press-event", G_CALLBACK (cb_cambia_color_fondo), (gpointer)colores[e]);
	}
	
	/* Los botones especiales selccionadores de color */
	/* El color de frente */
	toolcolor = gtk_tool_item_new ();
	color_button_fg =  gtk_color_button_new ();
	gtk_widget_set_tooltip_text(color_button_fg, "Color de frente");
	
	gdk_color_parse ("black", &color);
	gtk_color_button_set_color (GTK_COLOR_BUTTON (color_button_fg), &color);
	g_signal_connect (color_button_fg, "color-set", G_CALLBACK (cb_color_frente), NULL);
	
	gtk_container_add (GTK_CONTAINER (toolcolor), color_button_fg);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), toolcolor, -1);
	gtk_container_child_set (GTK_CONTAINER (toolgroup), GTK_WIDGET (toolcolor), "new-row", TRUE, NULL);
	
	/* El color de fondo */
	toolcolor = gtk_tool_item_new ();
	color_button_bg =  gtk_color_button_new ();
	gtk_widget_set_tooltip_text(color_button_bg, "Color de fondo");
	
	gdk_color_parse ("white", &color);
	gtk_color_button_set_color (GTK_COLOR_BUTTON (color_button_bg), &color);
	g_signal_connect (color_button_bg, "color-set", G_CALLBACK (cb_color_fondo), NULL);
	
	gtk_container_add (GTK_CONTAINER (toolcolor), color_button_bg);
	gtk_tool_item_group_insert (GTK_TOOL_ITEM_GROUP (toolgroup), toolcolor, -1);
	
	label_posxy = gtk_label_new ("X: Y:");
	gtk_box_pack_start (GTK_BOX (vbox), label_posxy, FALSE, FALSE, 0);
}

int setup_SDL (void) {
	int depth;
	if (glib_sdl_init (SDL_INIT_VIDEO) < 0) {
		exit_failure("Error init SDL.");
	}
	
	depth = SDL_VideoModeOK (SCREEN_WIDTH, SCREEN_HEIGHT, 24, 0);
	main_screen = depth ? SDL_SetVideoMode (SCREEN_WIDTH, SCREEN_HEIGHT, depth, 0) : NULL;
	
	if (main_screen == NULL) {
		exit_failure("Error init video.");
	}
	// Set SDL window title
	SDL_WM_SetCaption ("Paint GTK/SDL", NULL);

	// Set bg_color to white 
	color_bg = SDL_MapRGB (main_screen->format, 255, 255, 255);
	color_fg = SDL_MapRGB (main_screen->format, 0, 0, 0);
    SDL_FillRect (main_screen, NULL, color_bg);
}

void setup_globals (void) {
	// Set the initial mode
	mode = MODE_CURSOR;
	// Alloc & set initial values for main figure data
	draw_data = (DrawState *) malloc (sizeof (DrawState));
	if(draw_data == NULL) exit_failure("Error malloc.");
	draw_data->n_sides = 5; /* FIXME: El poligono debe arreglarse solo */
	draw_data->active = 0;
	
	// Create drawable surface from main screen surface
	draw_screen = SDL_CreateRGBSurface (main_screen->flags | SDL_SWSURFACE, main_screen->w, main_screen->h, main_screen->format->BitsPerPixel, main_screen->format->Rmask, main_screen->format->Gmask, main_screen->format->Bmask, main_screen->format->Amask);
	
    SDL_FillRect (draw_screen, NULL, color_bg);
}

int main (int argc, char *argv[]) {
	GtkWidget *vbox;
	
	srand ((unsigned int) time (NULL));
	
	gtk_init (&argc, &argv);
	
	main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (main_window), "Herramientas");
	gtk_window_set_default_size (GTK_WINDOW (main_window), 100, 300); /* FIXME: Tamaños... */
	gtk_window_set_focus_on_map (GTK_WINDOW (main_window), TRUE);
	gtk_window_set_type_hint (GTK_WINDOW (main_window), GDK_WINDOW_TYPE_HINT_UTILITY);
	gtk_window_set_keep_above (GTK_WINDOW (main_window), TRUE);
	g_signal_connect (main_window, "delete-event", G_CALLBACK (destroy), NULL);
	
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (main_window), vbox);
	
	//Setup SDL, globals, toolbar
	setup_SDL ();
	setup_globals ();
	setup_toolbar (vbox);
	
	g_timeout_add (200, cb_timer, NULL);
	gtk_widget_show_all (main_window);
	
	sdl_signal_connect (sdl_event, NULL);
	
	gtk_main ();
	
	return 0;
}