/*
 * polygon.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <math.h>
#include <stdio.h>

#include "api-paint.h"
#include "polygon.h"
#include "line.h"

void recalc_polygon (DrawState *draw_data) {
	int dx, dy;
	
	dx = draw_data->x_final - draw_data->x_initial;
	dy = draw_data->y_final - draw_data->y_initial;
	
	if (draw_data->mode & CENTER) {
		draw_data->radio_x = (int) sqrt ((dx * dx) + (dy * dy));
		
		draw_data->angle = (int) round (atan2 (dy, dx) / M_PI * 180.0);
		
		draw_data->x1 = draw_data->x_initial;
		draw_data->y1 = draw_data->y_initial;
	} else {
		if (abs (dx) > abs (dy)) {
			draw_data->radio_x = abs (dx) / 2;
		} else {
			draw_data->radio_x = abs (dy) / 2;
		}
		
		/* Cuando no está centrado, conservar los signos de los diferenciales */
		if (dx < 0) draw_data->x1 = draw_data->x_initial - draw_data->radio_x;
		else draw_data->x1 = draw_data->x_initial + draw_data->radio_x;
		if (dy < 0) draw_data->y1 = draw_data->y_initial - draw_data->radio_x;
		else draw_data->y1 = draw_data->y_initial + draw_data->radio_x;
		
		if (draw_data->n_sides % 2 == 1) {
			draw_data->angle = 270;
		} else {
			draw_data->angle = 270 - (180 / draw_data->n_sides);
		}
	}
}

void polygon_mouse_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	SDLMod mod;
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (draw_data->active) return;
	
	draw_data->active = 1;
	
	draw_data->x_initial = draw_data->x_final = event->button.x;
	draw_data->y_initial = draw_data->y_final = event->button.y;
	draw_data->radio_x = 0;
	
	draw_data->mode = 0;
	
	mod = SDL_GetModState ();
	
	if (mod & KMOD_CTRL) {
		draw_data->mode |= CENTER;
	}
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	polygon (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->radio_x, draw_data->n_sides, 0.0);
}

void polygon_mouse_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->motion.x;
	draw_data->y_final = event->motion.y;
	
	recalc_polygon (draw_data);
	
	draw_data->active = 0;
	
	polygon (draw_screen, foreground, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->n_sides, draw_data->angle);
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
}

void polygon_mouse_motion (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->motion.x;
	draw_data->y_final = event->motion.y;
	
	recalc_polygon (draw_data);
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	polygon (screen, foreground, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->n_sides, draw_data->angle);
}

void polygon_key_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	SDLKey key;
	int touched = 0;
	
	key = event->key.keysym.sym;
	
	switch (key) {
		case SDLK_RCTRL:
		case SDLK_LCTRL:
			draw_data->mode |= CENTER;
			touched = 1;
			break;
		case SDLK_3:
			draw_data->n_sides = 3;
			touched = 1;
			break;
		case SDLK_4:
			draw_data->n_sides = 4;
			touched = 1;
			break;
		case SDLK_5:
			draw_data->n_sides = 5;
			touched = 1;
			break;
		case SDLK_6:
			draw_data->n_sides = 6;
			touched = 1;
			break;
		case SDLK_7:
			draw_data->n_sides = 7;
			touched = 1;
			break;
		case SDLK_8:
			draw_data->n_sides = 8;
			touched = 1;
			break;
	}
	
	if (touched && draw_data->active) {
		recalc_polygon (draw_data);
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		polygon (screen, foreground, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->n_sides, draw_data->angle);
	}
}

void polygon_key_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	SDLKey key;
	
	key = event->key.keysym.sym;
	
	switch (key) {
		case SDLK_RCTRL:
		case SDLK_LCTRL:
			draw_data->mode &= ~CENTER;
			break;
	}
	if (draw_data->active) {
		recalc_polygon (draw_data);
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		polygon (screen, foreground, draw_data->x1, draw_data->y1, draw_data->radio_x, draw_data->n_sides, draw_data->angle);
	}
}

void polygon (SDL_Surface *surface, Uint32 color, int x, int y, int radio, int n_sides, int angle_initial) {
	double degrees = 360.0;
	int angle_temp;
	int x1, x2, y1, y2;
	int g;
	
	degrees /= (double) n_sides;
	
	for (g = 0; g < n_sides; g++) {
		angle_temp = (degrees * g + angle_initial);
		
		x1 = x + cos (angle_temp / 180.0 * M_PI) * radio;
		y1 = y + sin (angle_temp / 180.0 * M_PI) * radio;
		
		angle_temp += degrees;
		x2 = x + cos (angle_temp / 180.0 * M_PI) * radio;
		y2 = y + sin (angle_temp / 180.0 * M_PI) * radio;
		
		line (surface, color, x1, y1, x2, y2);
	}
}

