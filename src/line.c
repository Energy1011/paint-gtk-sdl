/*
 * line.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <math.h>

#include <stdarg.h>

#include "line.h"
#include "api-paint.h"

void line_mouse_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (draw_data->active != 0) return;
	
	draw_data->active = 1;
	
	draw_data->x_initial = draw_data->x_final = event->button.x;
	draw_data->y_initial = draw_data->y_final = event->button.y;
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	line (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
}

void line_mouse_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->button.x;
	draw_data->y_final = event->button.y;
	
	line (draw_screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	
	draw_data->active = 0;
}

void line_mouse_motion (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->button.x;
	draw_data->y_final = event->button.y;
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	line (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
}

#define SWAP(a, b, t) ((t) = (a), (a) = (b), (b) = (t))

void line (SDL_Surface *surface, Uint32 color, int x1, int y1, int x2, int y2) {
	int delta_x;
	int delta_y;
	int double_delta;
	int dif_delta;
	int p;
	int t;
	int increment;
	
	delta_x = x2 - x1;
	delta_y = y2 - y1;
	
	if ((delta_x > 0 && delta_y > 0) || (delta_x < 0 && delta_y < 0)) {
		increment = 1;
	} else {
		increment = -1;
	}
	
	delta_x = abs (delta_x);
	delta_y = abs (delta_y);
	
	if (delta_x > delta_y) {
		/* Switch points */
		if (x1 > x2) {
			SWAP (x1, x2, t);
			SWAP (y1, y2, t);
		}
		double_delta = delta_y * 2;
		dif_delta = double_delta - (2 * delta_x);
		p = double_delta - delta_x;
		
		drawpixel (surface, color, x1, y1);
		while (x1 < x2) {
			x1++;
			
			if (p < 0) {
				p = p + double_delta;
			} else {
				p = p + dif_delta;
				y1 = y1 + increment;
			}
			drawpixel (surface, color, x1, y1);
		}
	} else {
		/* Switch points */
		if (y1 > y2) {
			SWAP (x1, x2, t);
			SWAP (y1, y2, t);
		}
		double_delta = delta_x * 2;
		dif_delta = double_delta - (2 * delta_y);
		p = double_delta - delta_y;
		
		drawpixel (surface, color, x1, y1);
		while (y1 < y2) {
			y1++;
			
			if (p < 0) {
				p = p + double_delta;
			} else {
				p = p + dif_delta;
				x1 = x1 + increment;
			}
			drawpixel (surface, color, x1, y1);
		}
	}
}

void bresenham_ejecutable (SDL_Surface *screen, Uint32 color, int x1, int y1, int x2, int y2, void (*funcion)(SDL_Surface *, Uint32, int, int, va_list), ...) {
	int delta_x;
	int delta_y;
	int doble_delta;
	int diferencia_delta;
	int p, t;
	int incremento;
	
	va_list args, args_f;
	va_start (args, funcion);
	
	delta_x = x2 - x1;
	delta_y = y2 - y1;
	
	if ((delta_x > 0 && delta_y > 0) || (delta_x < 0 && delta_y < 0)) {
		incremento = 1;
	} else {
		incremento = -1;
	}
	
	delta_x = abs (delta_x);
	delta_y = abs (delta_y);
	
	if (delta_x > delta_y) {
		/* Acomodar los puntos */
		if (x1 > x2) {
			SWAP (x1, x2, t);
			SWAP (y1, y2, t);
		}
		doble_delta = delta_y * 2;
		diferencia_delta = doble_delta - (2 * delta_x);
		p = doble_delta - delta_x;
		
		va_copy (args_f, args);
		funcion (screen, color, x1, y1, args_f);
		va_end (args_f);
		while (x1 < x2) {
			x1++;
			
			if (p < 0) {
				p = p + doble_delta;
			} else {
				p = p + diferencia_delta;
				y1 = y1 + incremento;
			}
			va_copy (args_f, args);
			funcion (screen, color, x1, y1, args_f);
			va_end (args_f);
		}
	} else {
		/* Acomodar los puntos */
		if (y1 > y2) {
			SWAP (x1, x2, t);
			SWAP (y1, y2, t);
		}
		doble_delta = delta_x * 2;
		diferencia_delta = doble_delta - (2 * delta_y);
		p = doble_delta - delta_y;
		
		va_copy (args_f, args);
		funcion (screen, color, x1, y1, args_f);
		va_end (args_f);
		while (y1 < y2) {
			y1++;
			
			if (p < 0) {
				p = p + doble_delta;
			} else {
				p = p + diferencia_delta;
				x1 = x1 + incremento;
			}
			va_copy (args_f, args);
			funcion (screen, color, x1, y1, args_f);
			va_end (args_f);
		}
	}
	
	va_end (args);
}

#define ipart_(X) ((int)(X))
#define round_(X) ((int)(((double)(X))+0.5))
#define fpart_(X) (((double)(X))-(double)ipart_(X))
#define rfpart_(X) (1.0-fpart_(X))

void linea_antialisada (SDL_Surface *surface, Uint32 color, int x1, int y1, int x2, int y2) {
	double dx, dy;
	double gradient, xend, yend, xgap, ygap;
	int xpxl1, ypxl1, xpxl2, ypxl2, x, y;
	double intery, interx;
	double s;
	
	dx = (double) x2 - (double) x1;
	dy = (double) y2 - (double) y1;
	
	if (fabs(dx) > fabs(dy)) {
		if (x2 < x1) {
			SWAP (x1, x2, s);
			SWAP (y1, y2, s);
		}
	
		gradient = dy / dx;
		xend = round_(x1);
		yend = y1 + gradient*(xend - x1);
		xgap = rfpart_(x1 + 0.5);
		xpxl1 = xend;
		ypxl1 = ipart_(yend);
		drawpixel_trans (surface, color, xpxl1, ypxl1, rfpart_(yend) * xgap);
		drawpixel_trans (surface, color, xpxl1, ypxl1 + 1, fpart_(yend) * xgap);
		intery = yend + gradient;
	
		xend = round_(x2);
		yend = y2 + gradient * (xend - x2);
		xgap = fpart_(x2 + 0.5);
		xpxl2 = xend;
		ypxl2 = ipart_(yend);
		drawpixel_trans (surface, color, xpxl2, ypxl2, rfpart_(yend) * xgap);
		drawpixel_trans (surface, color, xpxl2, ypxl2 + 1, fpart_(yend) * xgap);

		for(x = xpxl1 + 1;x <= (xpxl2-1); x++) {
			drawpixel_trans (surface, color, x, ipart_(intery), rfpart_(intery));
			drawpixel_trans (surface, color, x, ipart_(intery) + 1, fpart_(intery));
			intery += gradient;
		}
	} else {
		if (y2 < y1) {
			SWAP(x1, x2, s);
			SWAP(y1, y2, s);
		}
		gradient = dx / dy;
		yend = round_(y1);
		xend = x1 + gradient * (yend - y1);
		ygap = rfpart_(y1 + 0.5);
		ypxl1 = yend;
		xpxl1 = ipart_(xend);
		drawpixel_trans (surface, color, xpxl1, ypxl1, rfpart_(xend) * ygap);
		drawpixel_trans (surface, color, xpxl1, ypxl1+1, fpart_(xend) * ygap);
		interx = xend + gradient;

		yend = round_(y2);
		xend = x2 + gradient * (yend - y2);
		ygap = fpart_(y2  + 0.5);
		ypxl2 = yend;
		xpxl2 = ipart_(xend);
		drawpixel_trans (surface, color, xpxl2, ypxl2, rfpart_(xend) * ygap);
		drawpixel_trans (surface, color, xpxl2, ypxl2 + 1, fpart_(xend) * ygap);

		for(y = ypxl1 + 1;y <= (ypxl2 - 1); y++) {
			drawpixel_trans (surface, color, ipart_(interx), y, rfpart_(interx));
			drawpixel_trans (surface, color, ipart_(interx) + 1, y, fpart_(interx));
			interx += gradient;
		}
	}
}

