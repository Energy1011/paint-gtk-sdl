/*
 * mano.h
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MANO_ALZADA_H__
#define __MANO_ALZADA_H__

#include <SDL.h>
#include <stdarg.h>

#include "api-paint.h"

void mano_alzada_mouse_down (SDL_Surface *, SDL_Surface *, Uint32, Uint32, SDL_Event *, DrawState *);
void mano_alzada_mouse_motion (SDL_Surface *, SDL_Surface *, Uint32, Uint32, SDL_Event *, DrawState *);
void mano_alzada_mouse_up (SDL_Surface *, SDL_Surface *, Uint32, Uint32, SDL_Event *, DrawState *);

#endif /* __MANO_ALZADA_H__ */

