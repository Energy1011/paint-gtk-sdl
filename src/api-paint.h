/*
 * api-paint.h
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __API_PAINT_H__
#define __API_PAINT_H__

#include <SDL.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

/* Comun flags */
#define CENTER 0x01
#define CIRCLE 0x02

/* Enum paint and tool functions */
enum {
	MODE_CURSOR, //Just cursor, Do Nothing especial
	MODE_LINE,
	MODE_CIRCLE_ELIPSE,
	MODE_RECT,
	MODE_POLYGON,
	MODE_SPLINE,
	MODE_PENCIL,
	MODE_ERASE,
	MODE_SPRAY,
	MODE_FILL,
	MODE_CUT,
	NUM_MODES
};

// Main strcut data to all functions-figures
typedef struct {
	int active;
	int radio_x, radio_y;
	int angle;
	int dx, dy;
	int mode;
	union {
		int state;
		int n_sides;
	};
	union { /* Algunos enteros disponibles para todos */
		struct {
			int x_array[6];
			int y_array[6];
		};
		struct {
			int x_initial, x1, x2, x_final, x3, x4;
			int y_initial, y1, y2, y_final, y3, y4;
		};
	};
} DrawState;

/* Las funciones siempre toman la pantalla principal, la pantalla de dibujado y la estructura con su estado */
typedef void (*PaintFunc) (SDL_Surface *, SDL_Surface *, Uint32, Uint32, SDL_Event *, DrawState *);

/* Prototipos de función disponibles para todos */
void drawpixel (SDL_Surface *surface, Uint32 color, int x, int y);
Uint32 getpixel (SDL_Surface *surface, int x, int y);
void drawpixel_trans (SDL_Surface *, Uint32, int, int, double);

#endif /* __API_PAINT_H__ */

