/*
 * glib-sdl.h
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GLIB_SDL_H__
#define __GLIB_SDL_H__

#include <glib-object.h>
#include <SDL_events.h>

G_BEGIN_DECLS

typedef gboolean (*GlibSdlEventFunc) (SDL_Event *event, gpointer data);

/* Prototipos de función públicos */
int glib_sdl_init (Uint32);
void sdl_signal_connect (GlibSdlEventFunc func, gpointer data);
void glib_sdl_quit (void);

G_END_DECLS

#endif /* __GLIB_SDL_H__ */

