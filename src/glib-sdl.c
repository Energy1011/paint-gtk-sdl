/*
 * glib-sdl.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib-object.h>
#include <SDL.h>

#include "glib-sdl.h"

typedef struct {
	GSource source;
	GlibSdlEventFunc func;
	gpointer data;
} GlibSdlSource;

/* Prototipos de función locales */
gboolean sdl_source_prepare (GSource *source, gint *timeout);
gboolean sdl_source_check (GSource *source);
gboolean sdl_source_dispatch (GSource *source, GSourceFunc func, gpointer data);

static GSourceFuncs SdlPollFuncs = {
	sdl_source_prepare,
	sdl_source_check,
	sdl_source_dispatch,
	NULL
};

GlibSdlSource *sdl_source = NULL;

gboolean sdl_source_prepare (GSource *source, gint *timeout) {
	SDL_Event vacio;
	int num_eventos;
	
	SDL_PumpEvents ();
	num_eventos = SDL_PeepEvents (&vacio, 1, SDL_PEEKEVENT, SDL_ALLEVENTS);
	
	if (num_eventos == 1) {
		*timeout = 0;
		return TRUE;
	}
	
	if (num_eventos == -1) {
		g_warning ("Error al hacer Polling a SDL: %s", SDL_GetError ());
	}
	
	*timeout = 50;
	return FALSE;
}

gboolean sdl_source_check (GSource *source) {
	SDL_Event vacio;
	int num_eventos;
	
	SDL_PumpEvents ();
	num_eventos = SDL_PeepEvents (&vacio, 1, SDL_PEEKEVENT, SDL_ALLEVENTS);
	
	if (num_eventos == 1) return TRUE;
	
	if (num_eventos == -1) {
		g_warning ("Error al hacer Polling a SDL: %s", SDL_GetError ());
	}
	
	return FALSE;
}

#define SDL_MAXEVENTS 20

gboolean sdl_source_dispatch (GSource *source, GSourceFunc func, gpointer data) {
	GlibSdlSource *local_sdl_source = (GlibSdlSource *) source;
	
	SDL_Event events[SDL_MAXEVENTS], *event;
	int num_eventos;
	gboolean handled = FALSE;
	
	num_eventos = SDL_PeepEvents (events, SDL_MAXEVENTS, SDL_GETEVENT, SDL_ALLEVENTS);
	
	if (num_eventos < 0) {
		g_warning ("Error al hacer Polling a SDL: %s", SDL_GetError ());
		return TRUE;
	}
	
	for (event = events; event < events + num_eventos; event++) {
		if (local_sdl_source->func != NULL) {
			handled = local_sdl_source->func (event, local_sdl_source->data);
		}
		
		if (!handled && event->type == SDL_QUIT) {
			SDL_Quit ();
			return FALSE;
		}
	}
	
	return TRUE;
}

int glib_sdl_init (Uint32 flags) {
	GSource *source;
	GMainContext *contexto_gtk;
	
	if (SDL_Init (flags) < 0) {
		g_warning ("Unable to init SDL: %s", SDL_GetError ());
		return -1;
	}
	
	contexto_gtk = g_main_context_default ();
	
	source = g_source_new (&SdlPollFuncs, sizeof (GlibSdlSource));
	
	sdl_source = (GlibSdlSource *) source;
	sdl_source->func = NULL;
	sdl_source->data = NULL;
	
	g_source_attach (source, contexto_gtk);
	g_source_unref (source);
	
	return 0;
}

void sdl_signal_connect (GlibSdlEventFunc func, gpointer data) {
	sdl_source->func = func;
	sdl_source->data = data;
}

void glib_sdl_quit (void) {
	g_source_destroy ((GSource *) sdl_source);
	SDL_Quit ();
}

