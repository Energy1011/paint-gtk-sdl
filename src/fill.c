/*
 * fill.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>

#include "api-paint.h"
#include "fill.h"

#define MAX_STACK 300

int stack_x [MAX_STACK];
int stack_y [MAX_STACK];
int stack_n;

void push_stack (int x, int y) {
	if (stack_n == MAX_STACK) {
		printf ("Desbordamiento\n");
		return;
	}
	
	stack_x[stack_n] = x;
	stack_y[stack_n++] = y;
}

int pop_stack (int *x, int *y) {
	if (stack_n == 0) {
		return 0;
	}
	
	*x = stack_x[--stack_n];
	*y = stack_y[stack_n];
	return 1;
}

void fill_mouse_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (draw_data->active) return;
	
	/* Draw the new color */
	draw_data->active = 1;
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	drawpixel (screen, foreground, event->button.x, event->button.y);
}

void fill_mouse_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	Uint32 old_color;
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->active = 0;
	old_color = getpixel (draw_screen, event->button.x, event->button.y);
	floodFillScanlineStack (draw_screen, event->button.x, event->button.y, old_color, foreground);
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
}

void floodFillScanlineStack (SDL_Surface *screen, int x, int y, Uint32 old_color, Uint32 new_color) {
	int y1, span_left, span_right;
	
	if (old_color == new_color) return;
	stack_n = 0;
	
	push_stack (x, y);
	
	while (pop_stack (&x, &y)) {
		
		y1 = y;
		while (y1 >= 0 && getpixel (screen, x, y1) == old_color) y1--;
		y1++;
		span_left = span_right = 0;
		while (y1 < SCREEN_HEIGHT && getpixel (screen, x, y1) == old_color) {
			drawpixel (screen, new_color, x, y1);
			
			if (!span_left && x > 0 && getpixel (screen, x - 1, y1) == old_color) {
				push_stack (x - 1, y1);
				span_left = 1;
			} else if (span_left && x > 0 && getpixel (screen, x - 1, y1) != old_color) {
				span_left = 0;
			}
			
			if (!span_right && x < SCREEN_WIDTH - 1 && getpixel (screen, x + 1, y1) == old_color) {
				push_stack (x + 1, y1);
				span_right = 1;
			} else if (span_right && x < SCREEN_WIDTH - 1 && getpixel (screen, x + 1, y1) != old_color) {
				span_right = 0;
			}
			y1++;
		}
	}
}
