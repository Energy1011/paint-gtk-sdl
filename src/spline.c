/*
 * spline.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <math.h>

#include "api-paint.h"
#include "spline.h"
#include "line.h"

void spline_mouse_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (!draw_data->active) {
		/* Init a new spline */
		draw_data->state = 0;
		
		draw_data->x_initial = draw_data->x_final = event->button.x;
		draw_data->y_initial = draw_data->y_final = event->button.y;
		
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		line (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
		
		draw_data->active = 1;
	} else {
		if (draw_data->state == 1) {
			draw_data->state++;
			
			draw_data->x1 = draw_data->x2 = event->button.x;
			draw_data->y1 = draw_data->y2 = event->button.y;
			
			SDL_BlitSurface (draw_screen, NULL, screen, NULL);
			spline (screen, foreground, draw_data->x_array, draw_data->y_array);
		} else if (draw_data->state == 3) {
			draw_data->state++;
			
			draw_data->x2 = event->button.x;
			draw_data->y2 = event->button.y;
			
			SDL_BlitSurface (draw_screen, NULL, screen, NULL);
			spline (screen, foreground, draw_data->x_array, draw_data->y_array);
		}
	}
}

void spline_mouse_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (!draw_data->active) return;
	
	if (draw_data->state == 0) {
		draw_data->x_final = event->button.x;
		draw_data->y_final = event->button.y;
		draw_data->state++;
	} else if (draw_data->state == 2) {
		draw_data->x1 = draw_data->x2 = event->button.x;
		draw_data->y1 = draw_data->y2 = event->button.y;
		draw_data->state++;
	} else if (draw_data->state == 4) {
		draw_data->x2 = event->button.x;
		draw_data->y2 = event->button.y;
	}
	
	if (draw_data->state == 0 || draw_data->state == 1) {
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		line (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
	} else if (draw_data->state == 4) {
		spline (draw_screen, foreground, draw_data->x_array, draw_data->y_array);
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		draw_data->active = 0;
	} else {
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		spline (screen, foreground, draw_data->x_array, draw_data->y_array);
	}
}

void spline_mouse_motion (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	if (draw_data->state == 0) {
		draw_data->x_final = event->motion.x;
		draw_data->y_final = event->motion.y;
	} else if (draw_data->state == 2) {
		draw_data->x1 = draw_data->x2 = event->motion.x;
		draw_data->y1 = draw_data->y2 = event->motion.y;
	} else if (draw_data->state == 4) {
		draw_data->x2 = event->motion.x;
		draw_data->y2 = event->motion.y;
	}
	
	if (draw_data->state == 0 || draw_data->state == 1) {
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		line (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
	} else {
		SDL_BlitSurface (draw_screen, NULL, screen, NULL);
		spline (screen, foreground, draw_data->x_array, draw_data->y_array);
	}
}

void spline (SDL_Surface *screen, Uint32 color, int *x, int *y) {
	double time;
	int xf, yf;
	
	for (time = 0; time <= 1.0; time += 0.001) {
		xf = pow (1-time,3) * x[0] +
		3 * time * pow (1-time, 2) * x[1] +
		3 * pow (time,2) * (1-time) * x[2] +
		pow (time,3) * x[3];
		
		yf = pow (1-time,3) * y[0] +
		3 * time * pow (1-time,2) * y[1] +
		3 * pow (time,2) * (1-time) * y[2] +
		pow (time,3) * y[3];
		
		drawpixel (screen, color, xf, yf);
	}
}

