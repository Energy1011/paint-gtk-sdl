/*
 * mano.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <stdarg.h>

#include "api-paint.h"
#include "mano.h"
#include "line.h"

void mano_alzada_mouse_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (estado->active) return;
	
	/* Procesar este método */
	estado->active = 1;
	
	drawpixel (dibujo, frente, evento->button.x, evento->button.y);
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
}

void mano_alzada_mouse_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!estado->active) return;
	
	drawpixel (dibujo, frente, evento->button.x, evento->button.y);
	SDL_BlitSurface (dibujo, NULL, screen, NULL);
	
	estado->active = 0;
}

void mano_alzada_mouse_motion (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (estado->active) {
		if (evento->motion.xrel == 0 && evento->motion.yrel == 0) {
			drawpixel (dibujo, frente, evento->button.x, evento->button.y);
		} else {
			linea_antialisada (dibujo, frente, evento->motion.x - evento->motion.xrel, evento->motion.y - evento->motion.yrel, evento->motion.x, evento->motion.y);
		}
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
	}
	/* TODO: Dibujar el lapiz, si así se desea */
}

