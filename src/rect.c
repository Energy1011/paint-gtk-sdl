/*
 * rect.c
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>

#include "rect.h"
#include "api-paint.h"

void rect_mouse_down (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (draw_data->active != 0) return;
	
	draw_data->active = 1;
	
	draw_data->x_initial = event->button.x;
	draw_data->y_initial = event->button.y;
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	rect (screen, foreground, draw_data->x_initial, draw_data->y_initial, event->motion.x, event->motion.y);
}

void rect_mouse_up (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if (event->button.button != SDL_BUTTON_LEFT) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->button.x;
	draw_data->y_final = event->button.y;
	
	rect (draw_screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	
	draw_data->active = 0;
}

void rect_mouse_motion (SDL_Surface *screen, SDL_Surface *draw_screen, Uint32 foreground, Uint32 background, SDL_Event *event, DrawState *draw_data) {
	if ((event->motion.state & SDL_BUTTON_LEFT) == 0) return;
	if (!draw_data->active) return;
	
	draw_data->x_final = event->button.x;
	draw_data->y_final = event->button.y;
	
	SDL_BlitSurface (draw_screen, NULL, screen, NULL);
	rect (screen, foreground, draw_data->x_initial, draw_data->y_initial, draw_data->x_final, draw_data->y_final);
}


void rect (SDL_Surface *screen, Uint32 color, int x1, int y1, int x2, int y2) {
	// top
    line(screen, color, x1, y1, x2, y1);
    // right
    line(screen, color, x2, y1, x2, y2);
    // bottom
    line(screen, color, x2, y2, x1, y2);
    // left    
    line(screen, color, x1, y2, x1, y1);
}
