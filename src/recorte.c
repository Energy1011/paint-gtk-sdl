/*
 * recorte.h
 * This file is part of Paint GTK+ SDL
 *
 * Copyright (C) 2013 - Félix Arreola Rodríguez
 * Copyright (C) 2013 - Alejandro Anaya (energy1011@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <SDL.h>
#include <math.h>

#include "api-paint.h"
#include "recorte.h"

void recorte_mouse_down (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!estado->active) {
		estado->state = 0;
		
		estado->x_initial = estado->x_final = evento->button.x;
		estado->y_initial = estado->y_final = evento->button.y;
		estado->active = 1;
	} else {
		if (estado->state == 1) { /* Hay una selección activa, verificar si está dentro del rectangulo */
			if (estado->x_initial == estado->x_final ||
			    estado->y_initial == estado->y_final) {
				/* Zona demasiado pequeña, iniciar una nueva */
				estado->state = 0;
				estado->x_initial = estado->x_final = evento->button.x;
				estado->y_initial = estado->y_final = evento->button.y;
			} else if (estado->x_initial > evento->button.x || estado->x_final < evento->button.x ||
				estado->y_initial > evento->button.y || estado->y_final < evento->button.y) {
				/* Fuera de la zona de selección, comenzar una nueva zona */
				estado->state = 0;
				estado->x_initial = estado->x_final = evento->button.x;
				estado->y_initial = estado->y_final = evento->button.y;
			} else {
				/* Esta dentro de la zona de selección e hizo mouse down */
				/* Recalcular los offsets, cambiar de estado */
				estado->dx = evento->button.x - estado->x_initial;
				estado->dy = evento->button.y - estado->y_initial;
				
				estado->state = 2;
			}
		}
	}	
}

void recorte_mouse_up (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (evento->button.button != SDL_BUTTON_LEFT) return;
	if (!estado->active) return;
	
	if (estado->state == 0) {
		Uint32 gris;
		int g, h;
		
		estado->x_final = evento->motion.x;
		estado->y_final = evento->motion.y;
		
		if (estado->x_initial == estado->x_final ||
			    estado->y_initial == estado->y_final) {
			estado->active = 0;
			estado->state = 0;
			SDL_BlitSurface (dibujo, NULL, screen, NULL);
			return;
		}
		
		/* Terminar de marcar la selección activa */
		estado->state = 1;
		
		if (estado->x_initial > estado->x_final) {
			estado->x1 = estado->x_final;
			estado->x2 = estado->x_initial;
		} else {
			estado->x1 = estado->x_initial;
			estado->x2 = estado->x_final;
		}
		
		if (estado->y_initial > estado->y_final) {
			estado->y1 = estado->y_final;
			estado->y2 = estado->y_initial;
		} else {
			estado->y1 = estado->y_initial;
			estado->y2 = estado->y_final;
		}
		
		gris = SDL_MapRGB (screen->format, 128, 128, 128);
		
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		for (g = estado->x1; g < estado->x2; g++) {
			for (h = estado->y1; h < estado->y2; h++) {
				drawpixel_trans (screen, gris, g, h, 0.5);
			}
		}
		
		line(screen, gris, estado->x1, estado->y1, estado->x2, estado->y1);
		line(screen, gris, estado->x2, estado->y1, estado->x2, estado->y2);
		line(screen, gris, estado->x2, estado->y2, estado->x1, estado->y2);
		line(screen, gris, estado->x1, estado->y2, estado->x1, estado->y1);
		
		estado->x_initial = estado->x1;
		estado->x_final = estado->x2;
		estado->y_initial = estado->y1;
		estado->y_final = estado->y2;
		
	} else if (estado->state == 2) {
		Uint32 gris;
		int g, h;
		/* Hay un mouse down, mover las cosas */
		SDL_Rect rect_origen, rect_destino;
		
		rect_destino.x = evento->button.x - estado->dx;
		rect_destino.y = evento->button.y - estado->dy;
		rect_origen.w = rect_destino.w = estado->x_final - estado->x_initial;
		rect_origen.h = rect_destino.h = estado->y_final - estado->y_initial;
		
		rect_origen.x = estado->x_initial;
		rect_origen.y = estado->y_initial;
		
		estado->x_initial = rect_destino.x;
		estado->y_initial = rect_destino.y;
		estado->x_final = rect_destino.x + rect_destino.w;
		estado->y_final = rect_destino.y + rect_destino.h;
		
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		SDL_FillRect (screen, &rect_origen, fondo);
		
		SDL_BlitSurface (dibujo, &rect_origen, screen, &rect_destino);
		SDL_BlitSurface (screen, NULL, dibujo, NULL);
		
		/* Dibujar la sombra de resalte */
		gris = SDL_MapRGB (screen->format, 128, 128, 128);
		
		for (g = rect_destino.x; g < rect_destino.x + rect_destino.w; g++) {
			for (h = rect_destino.y; h < rect_destino.y + rect_destino.h; h++) {
				drawpixel_trans (screen, gris, g, h, 0.5);
			}
		}
		
		line(screen, gris, rect_destino.x, rect_destino.y, rect_destino.x + rect_destino.w, rect_destino.y);
		line(screen, gris, rect_destino.x + rect_destino.w , rect_destino.y, rect_destino.x + rect_destino.w, rect_destino.y + rect_destino.h);
		line(screen, gris, rect_destino.x + rect_destino.w, rect_destino.y + rect_destino.h, rect_destino.x, rect_destino.y + rect_destino.h);
		line(screen, gris, rect_destino.x, rect_destino.y + rect_destino.h, rect_destino.x, rect_destino.y);
		estado->state = 1;
	}
}

void recorte_mouse_motion (SDL_Surface *screen, SDL_Surface *dibujo, Uint32 frente, Uint32 fondo, SDL_Event *evento, DrawState *estado) {
	if (!estado->active) return;
	
	if (estado->state == 0) {
		Uint32 gris;
		int g, h;
		
		estado->x_final = evento->motion.x;
		estado->y_final = evento->motion.y;
		
		if (estado->x_initial > estado->x_final) {
			estado->x1 = estado->x_final;
			estado->x2 = estado->x_initial;
		} else {
			estado->x1 = estado->x_initial;
			estado->x2 = estado->x_final;
		}
		
		if (estado->y_initial > estado->y_final) {
			estado->y1 = estado->y_final;
			estado->y2 = estado->y_initial;
		} else {
			estado->y1 = estado->y_initial;
			estado->y2 = estado->y_final;
		}
		
		gris = SDL_MapRGB (screen->format, 128, 128, 128);
		
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		for (g = estado->x1; g < estado->x2; g++) {
			for (h = estado->y1; h < estado->y2; h++) {
				drawpixel_trans (screen, gris, g, h, 0.5);
			}
		}
		
		line(screen, gris, estado->x1, estado->y1, estado->x2, estado->y1);
		line(screen, gris, estado->x2, estado->y1, estado->x2, estado->y2);
		line(screen, gris, estado->x2, estado->y2, estado->x1, estado->y2);
		line(screen, gris, estado->x1, estado->y2, estado->x1, estado->y1);
	} else if (estado->state == 2) {
		Uint32 gris;
		int g, h;
		/* Hay un mouse down, mover las cosas */
		SDL_Rect rect_origen, rect_destino;
		
		rect_destino.x = evento->motion.x - estado->dx;
		rect_destino.y = evento->motion.y - estado->dy;
		rect_origen.w = rect_destino.w = estado->x_final - estado->x_initial;
		rect_origen.h = rect_destino.h = estado->y_final - estado->y_initial;
		
		rect_origen.x = estado->x_initial;
		rect_origen.y = estado->y_initial;
		
		SDL_BlitSurface (dibujo, NULL, screen, NULL);
		SDL_FillRect (screen, &rect_origen, fondo);
		
		SDL_BlitSurface (dibujo, &rect_origen, screen, &rect_destino);
		
		/* Dibujar la sombra de resalte */
		gris = SDL_MapRGB (screen->format, 128, 128, 128);
		
		/*for (g = rect_destino.x; g < rect_destino.x + rect_destino.w; g++) {
			for (h = rect_destino.y; h < rect_destino.y + rect_destino.h; h++) {
				drawpixel_trans (screen, gris, g, h, 0.5);
			}
		}*/
		
		line(screen, gris, rect_destino.x, rect_destino.y, rect_destino.x + rect_destino.w, rect_destino.y);
		line(screen, gris, rect_destino.x + rect_destino.w , rect_destino.y, rect_destino.x + rect_destino.w, rect_destino.y + rect_destino.h);
		line(screen, gris, rect_destino.x + rect_destino.w, rect_destino.y + rect_destino.h, rect_destino.x, rect_destino.y + rect_destino.h);
		line(screen, gris, rect_destino.x, rect_destino.y + rect_destino.h, rect_destino.x, rect_destino.y);
	}
}

